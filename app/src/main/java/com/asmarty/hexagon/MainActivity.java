package com.asmarty.hexagon;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements RandomValueProvider.ValueListener {

    private HexagonProgressView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view = findViewById(R.id.hexagon);
        getLifecycle().addObserver(new RandomValueProvider(this));
    }

    @Override
    public void onValueUpdate(int value) {
        view.update(value);
    }
}
