package com.asmarty.hexagon;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR1;

/**
 * Created by borislavkalaydzhiev on 07.10.16.
 *
 * <p>A custom progress indicator in the form of a hexagon. Call {@link #update(int)} to set
 * the main value and update the view. In case of invalid values you can call an overloaded
 * update method like {@link #update(String)} to show a status text instead of the progress,
 * the validity of a value could be checked via {@link #isValueOk(int)}.
 */
public class HexagonProgressView extends View {

    // Ids of resources needed for the view's styling.
    // TODO: 13.10.16 Instead of hardcoding resources, provide them as attributes set in the xml.
    private static final @ColorRes int COLOR_BACKGROUND = R.color.divider;
    private static final @ColorRes int COLOR_FOREGROUND = R.color.colorBackground;
    private static final @ColorRes int COLOR_PRIMARY = R.color.colorPrimaryDark;
    private static final @ColorRes int COLOR_SECONDARY = R.color.textColorSecondary;

    private static final @StringRes int TEXT_UNIT = R.string.percent_sign;
    private static final @StringRes int TEXT_TITLE = R.string.percent;

    private static final int TEXT_SIZE_SP_VALUE = 112;
    private static final int TEXT_SIZE_SP_UNIT = 56;
    private static final int TEXT_SIZE_SP_TITLE = 24;
    private static final int TEXT_SIZE_SP_OPTION = 14;

    private static final int PROGRESS_VISUAL_WIDTH_DP = 10;
    private static final int BORDER_OVERLAY_WIDTH_DP = 4;

    private static final int VALUE_MIN = 0;
    private static final int VALUE_MAX = 100;
    private static final String VALUE_DEFAULT = "00";

    private static final float PROGRESS_IN_DEGREES = 360 / (float) VALUE_MAX;

    private final int SIZE_PX = (int) dpToPx(300);

    private final @ColorInt int colorBackground;
    private final @ColorInt int colorForeground;
    private final @ColorInt int colorPrimary;
    private final @ColorInt int colorSecondary;

    private final float textSizePxValue;
    private final float textSizePxUnit;
    private final float textSizePxTitle;
    private final float textSizePxOption;

    private final float progressVisualWidthPx;
    private final float borderOverlayWidthPx;

    private final List<PointF> majorBackgroundPoints = new ArrayList<>();
    private final List<PointF> majorForegroundPoints = new ArrayList<>();

    private final Path path = new Path();
    private final Paint paint = new Paint();
    private final RectF rectF = new RectF();
    private final Rect rect = new Rect();
    private final PathEffect innerRadius = new CornerPathEffect(10);
    private final PathEffect outerRadius = new CornerPathEffect(20);

    private String value = VALUE_DEFAULT;
    private String unit;
    private String title;
    private String line1;
    private String line2;

    private boolean line1Set;
    private boolean line2Set;

    private float progressInDegrees;

    // A border effect is created by drawing two layers - background and foreground.
    // They have same proportions but different side lengths and colors. The diff
    // between the length of the sides defines the thickness of the "border".
    private float sideLengthBackground;
    private float sideLengthForeground;

    // The background rect needs to be centered in a square rect. These are
    // the translations which need to be applied on the canvas in order to
    // center the background polygon.
    private float translationXBackground;
    private float translationYBackground;

    // It's the same for the foreground polygon.
    private float translationXForeground;
    private float translationYForeground;

    private float originXValue;
    private float originYValue;

    private float originXUnit;
    private float originYUnit;

    private float originXTitle;
    private float originYTitle;

    // Origins when in single line text mode - e.g. no progress available, just a hint.
    private float originXLine;
    private float originYLine;

    // Vertical origins when in two-line mode.
    private float originYLine1;
    private float originYLine2;

    private CustomFloatAnimator animator;

    {
        setSaveEnabled(true);
        colorBackground = getResources().getColor(COLOR_BACKGROUND);
        colorForeground = getResources().getColor(COLOR_FOREGROUND);
        colorPrimary = getResources().getColor(COLOR_PRIMARY);
        colorSecondary = getResources().getColor(COLOR_SECONDARY);
        textSizePxValue = dpToPx(TEXT_SIZE_SP_VALUE);
        textSizePxUnit = dpToPx(TEXT_SIZE_SP_UNIT);
        textSizePxTitle = dpToPx(TEXT_SIZE_SP_TITLE);
        textSizePxOption = dpToPx(TEXT_SIZE_SP_OPTION);
        progressVisualWidthPx = dpToPx(PROGRESS_VISUAL_WIDTH_DP);
        borderOverlayWidthPx = dpToPx(BORDER_OVERLAY_WIDTH_DP);
        unit = getResources().getString(TEXT_UNIT);
        title = getResources().getString(TEXT_TITLE);
        animator = new CustomFloatAnimator(this, 0);
//        getResources().getDimensionPixelSize(R.dimen.myFontSize);
    }

    public HexagonProgressView(Context context) {
        super(context);
    }

    public HexagonProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HexagonProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void update(int value) {
        if (!isValueOk(value)) {
            String msg = String.format(Locale.US, "Expected value in [%d, %d], current: %d",
                    VALUE_MIN,
                    VALUE_MAX,
                    value);
            throw new IllegalArgumentException(msg);
        }

        if (value == VALUE_MAX) {
            update("Done");
            return;
        }

        // TODO: 17.10.16 Improve, use a lookup table but do not create strings each time.
        this.value = value < 10 ? "0" + value : String.valueOf(value);
        progressInDegrees = value * PROGRESS_IN_DEGREES;
        updateAnimator(progressInDegrees);
        setLine1(null);
        setLine2(null);
        invalidate();
    }

    public void update(@NonNull String line1) {
        clearProgress();
        updateAnimator(0);
        setLine1(line1);
        setLine2(null);
        invalidate();
    }

    public void update(@NonNull String line1, @NonNull String line2) {
        clearProgress();
        updateAnimator(0);
        setLine1(line1);
        setLine2UpperCased(line2);
        invalidate();
    }

    public boolean isValueOk(int value) {
        return value >= VALUE_MIN && value <= VALUE_MAX;
    }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth = measureWidth(widthMeasureSpec);
        int measuredHeight = measureHeight(heightMeasureSpec);
        setMeasuredDimension(measuredWidth, measuredHeight);

        calcSideLengths(measuredWidth);
        calcTranslations();
        calcElementsPosition();

        calcMajorPoints(majorBackgroundPoints, sideLengthBackground);
        calcMajorPoints(majorForegroundPoints, sideLengthForeground);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        // Draws the background
        definePath(majorBackgroundPoints, path);
        prepareFillPaint(paint, colorBackground, null);
        canvas.save();
        canvas.translate(translationXBackground, translationYBackground); // center horizontally
        canvas.drawPath(path, paint);
        canvas.restore();

        // Draws the progress
        rectF.set(0, 0, getMeasuredWidth(), getMeasuredWidth());
        prepareFillPaint(paint, colorPrimary, null);
        canvas.drawArc(rectF, 270, tryGetAnimatedProgress(), true, paint);

        // Makes the arc even to the outer border of the background polygon
        canvas.save();
        canvas.translate(translationXBackground, translationYBackground); // center horizontally
        canvas.clipPath(path, Region.Op.DIFFERENCE);
        canvas.drawColor(colorForeground);
        canvas.restore();

        // Draws a border on top of the background so outer edges look rounded
        prepareStrokePaint(paint, colorForeground, outerRadius);
        canvas.save();
        canvas.translate(translationXBackground, translationYBackground); // center horizontally
        canvas.drawPath(path, paint);
        canvas.restore();

        // Draws the foreground
        definePath(majorForegroundPoints, path);
        prepareFillPaint(paint, colorForeground, innerRadius);
        canvas.save();
        canvas.translate(translationXForeground, translationYForeground);
        canvas.drawPath(path, paint);
        canvas.restore();

        // Draws either progress visuals or status texts.
        if (line1Set && line2Set) {
            drawAllTextLines(canvas);
        } else if (line1Set) {
            drawTextLine1(canvas);
        } else {
            drawProgress(canvas);
        }
    }

    private void drawProgress(Canvas canvas) {
        // Draws the value centered, almost perfectly centered in vertical
        prepareTextPaint(paint, colorPrimary, textSizePxValue, Paint.Align.CENTER);
        canvas.drawText(value, originXValue, originYValue, paint);
        // Draws the unit to the bottom right of the value
        prepareTextPaint(paint, colorPrimary, textSizePxUnit, Paint.Align.LEFT);
        canvas.drawText(unit, originXUnit, originYUnit, paint);
        // Draws a title above the value
        prepareTextPaint(paint, colorSecondary, textSizePxTitle, Paint.Align.LEFT);
        canvas.drawText(title, originXTitle, originYTitle, paint);
    }

    private void drawTextLine1(Canvas canvas) {
        //Draws a single line of text, it's value is set via the update(String, String) method.
        prepareTextPaint(paint, colorSecondary, textSizePxTitle, Paint.Align.CENTER);
        canvas.drawText(line1, originXLine, originYLine, paint);
    }

    private void drawAllTextLines(Canvas canvas) {
        // See drawTextLine1(Canvas)
        prepareTextPaint(paint, colorSecondary, textSizePxTitle, Paint.Align.CENTER);
        canvas.drawText(line1, originXLine, originYLine1, paint);
        // Additional line of text
        prepareTextPaint(paint, colorPrimary, textSizePxOption, Paint.Align.CENTER);
        canvas.drawText(line2.toUpperCase(), originXLine, originYLine2, paint);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        SavedState state = new SavedState(super.onSaveInstanceState());
        state.value = value;
        state.line1 = line1;
        state.line2 = line2;
        state.progressInDegrees = progressInDegrees;
        if (animator != null) {
            state.animatedProgressInDegrees = animator.getAnimatedValue();
        }
        return state;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        value = savedState.value;
        setLine1(savedState.line1);
        setLine2UpperCased(savedState.line2);
        progressInDegrees = savedState.progressInDegrees;
        if (animator != null) {
            animator.setStartValue(savedState.animatedProgressInDegrees);
        }
    }

    private void prepareFillPaint(Paint paint, @ColorInt int color, PathEffect pathEffect) {
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setPathEffect(pathEffect);
    }

    private void prepareStrokePaint(Paint paint, @ColorInt int color, PathEffect pathEffect) {
        prepareFillPaint(paint, color, pathEffect);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderOverlayWidthPx);
    }

    private void prepareTextPaint(Paint paint,
                                  @ColorInt int color,
                                  float textSize,
                                  Paint.Align align) {
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setTextSize(textSize);
        paint.setTextAlign(align);
        paint.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
    }

    private int measureWidth(int measureSpec) {
        int size;
        if (SDK_INT >= JELLY_BEAN_MR1) {
            size = SIZE_PX + getPaddingStart() + getPaddingEnd();
        } else {
            size = SIZE_PX + getPaddingLeft() + getPaddingRight();
        }
        return resolveSizeAndState(size, measureSpec, 0);
    }

    private int measureHeight(int measureSpec) {
        int size = SIZE_PX + getPaddingTop() + getPaddingBottom();
        return resolveSizeAndState(size, measureSpec, 0);
    }

    private void calcSideLengths(int measuredWidth) {
        sideLengthBackground = measuredWidth / 2;
        sideLengthForeground = sideLengthBackground - progressVisualWidthPx;
    }

    private void calcTranslations() {
        final float halfSqrt3 = (float) (Math.sqrt(3) / 2);
        translationXBackground = sideLengthBackground - sideLengthBackground * halfSqrt3;
        translationYBackground = 0;
        translationXForeground = sideLengthBackground - sideLengthForeground * halfSqrt3;
        translationYForeground = sideLengthBackground - sideLengthForeground;
    }

    private void calcMajorPoints(List<PointF> points, float sideLength ) {
        float sideSqrt3 = (float)(sideLength * Math.sqrt(3));
        float halfSideSqrt3 = sideSqrt3 / 2;
        float halfSide = sideLength / 2;
        float doubleSide = sideLength * 2;
        float sideAndAHalf = sideLength * 1.5f;

        points.clear();
        points.add(new PointF(halfSideSqrt3, 0));
        points.add(new PointF(sideSqrt3, halfSide));
        points.add(new PointF(sideSqrt3, sideAndAHalf));
        points.add(new PointF(halfSideSqrt3, doubleSide));
        points.add(new PointF(0, sideAndAHalf));
        points.add(new PointF(0, halfSide));
    }

    private void definePath(List<PointF> points, Path path) {
        boolean movedToPosition = false;
        path.reset();
        for (PointF point : points) {
            if (!movedToPosition) {
                movedToPosition = true;
                path.moveTo(point.x, point.y);
                continue;
            }
            path.lineTo(point.x, point.y);
        }
        path.close();
    }

    private void calcElementsPosition() {
        float twoDigitTextWidth = calcDefaultTwoDigitTextWidthInPx();

        originXValue = getMeasuredWidth() / 2;
        originYValue = (getMeasuredHeight() / 2) - ((paint.descent() + paint.ascent()) / 2);

        originXTitle = (getMeasuredWidth() - twoDigitTextWidth) / 2;
        originYTitle = getMeasuredHeight() / 4 + dpToPx(16);

        paint.setTextSize(textSizePxUnit);
        originXUnit = (getMeasuredWidth() / 2) + (twoDigitTextWidth / 2);
        originYUnit = (getMeasuredHeight() / 2)
                + ((Math.abs(paint.descent()) + Math.abs(paint.ascent())) / 2)
                + (paint.descent() / 3) ;

        originXLine = getMeasuredWidth() / 2;
        originYLine = getMeasuredHeight() / 2 + dpToPx(TEXT_SIZE_SP_OPTION) / 2;
        originYLine1 = getMeasuredHeight() / 2  - dpToPx(TEXT_SIZE_SP_OPTION);
        originYLine2 = getMeasuredHeight() / 2  + dpToPx(TEXT_SIZE_SP_TITLE);
    }

    private float calcDefaultTwoDigitTextWidthInPx() {
        final String widestTwoDigitNumber = "44";
        // Same paint as the one used to draw the two digit number.
        prepareTextPaint(paint, colorPrimary, textSizePxValue, Paint.Align.CENTER);
        rect.setEmpty();
        // Gets a Rect where the two digit number is drawn with specific size and font.
        // Basically all numbers are drawn centered in the Rect of number 44, measured
        // in pixels this is the widest number.
        paint.getTextBounds(widestTwoDigitNumber, 0, widestTwoDigitNumber.length(), rect);
        return rect.right - rect.left;
    }

    private void clearProgress() {
        value = VALUE_DEFAULT;
        progressInDegrees = 0;
    }

    private void setLine1(String line1) {
        if (!TextUtils.isEmpty(line1)){
            line1Set = true;
            this.line1 = line1;
        } else {
            line1Set = false;
            this.line1 = "";
        }
    }

    private void setLine2(String line2) {
        if (!TextUtils.isEmpty(line2)){
            line2Set = true;
            this.line2 = line2;
        } else {
            line2Set = false;
            this.line2 = "";
        }
    }

    private void setLine2UpperCased(String line2) {
        if (!TextUtils.isEmpty(line2)) {
            setLine2(line2.toUpperCase());
        }
    }

    private void updateAnimator(float value) {
        if (animator != null) {
            animator.animateTo(value);
        }
    }

    private float tryGetAnimatedProgress() {
        return animator != null && animator.isEnabled() ? animator.getAnimatedValue()
                : progressInDegrees;
    }

    private float dpToPx(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    private static class CustomFloatAnimator implements ValueAnimator.AnimatorUpdateListener {

        private final WeakReference<View> viewRef;
        private final ValueAnimator animator;
        private final boolean enabled;

        // Enclosing view is responsible to save/restore this state.
        // A value in between startValue and endValue,should be used
        // only in the drawing/updating of the next frame.
        private float animatedValue;

        public CustomFloatAnimator(View view, float startValue) {
            viewRef = new WeakReference<>(view);
            animator = ValueAnimator.ofFloat();
            animator.setDuration(500);
            animator.addUpdateListener(this);
            enabled = true;
            animatedValue = startValue;
        }

        public void setStartValue(float startValue) {
            animatedValue = startValue;
        }

        // Animates from the current animated value to the end value.
        public void animateTo(float endValue) {
            if (enabled) {
                if (animator.isRunning()) {
                    animator.cancel();
                }
                animator.setFloatValues(animatedValue, endValue);
                animator.start();
            }
        }

        public float getAnimatedValue() {
            return animatedValue;
        }

        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            animatedValue = (float) animation.getAnimatedValue();
            View view = viewRef.get();
            if (view != null) {
                view.invalidate();
            }
        }
    }

    private static class SavedState extends BaseSavedState {

        String value;
        String line1;
        String line2;
        float progressInDegrees;
        float animatedProgressInDegrees;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            value = in.readString();
            line1 = in.readString();
            line2 = in.readString();
            progressInDegrees = in.readFloat();
            animatedProgressInDegrees = in.readFloat();
        }

        @Override
        public void writeToParcel(@NonNull Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeString(value);
            out.writeString(line1);
            out.writeString(line2);
            out.writeFloat(progressInDegrees);
            out.writeFloat(animatedProgressInDegrees);
        }

        public static final Creator<SavedState> CREATOR
                = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
