package com.asmarty.hexagon;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Random;

/**
 * Provides random value in [0,100] every second
 */
public class RandomValueProvider implements LifecycleObserver {

    public interface ValueListener {
        void onValueUpdate(int value);
    }

    private static final int UPDATE_INTERVAL_MS = 1000;
    private static final int MAX_VALUE = 101;

    private final Handler handler;
    private final Runnable updateRunnable;

    public RandomValueProvider(@NonNull final ValueListener valueListener) {
        final Random random = new Random(System.currentTimeMillis());
        handler = new Handler();
        updateRunnable = new Runnable() {
            @Override
            public void run() {
                // Notify interested parties and schedule the next update
                int value = random.nextInt(MAX_VALUE);
                Log.d("RandomValueProvider", "valueUpdate: " + value);
                valueListener.onValueUpdate(value);
                handler.postDelayed(updateRunnable, UPDATE_INTERVAL_MS);
            }
        };
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        enable(true);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void stop() {
        enable(false);
    }

    private void enable(boolean enable) {
        if (enable) {
            handler.postDelayed(updateRunnable, UPDATE_INTERVAL_MS);
        } else {
            handler.removeCallbacks(updateRunnable);
        }
    }
}
